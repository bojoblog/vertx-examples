package fr.sewatech.vertx.file;

import io.vertx.core.buffer.Buffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;

import static fr.sewatech.vertx.file.ReadFileApplication.vertx;
import static java.lang.Integer.min;

public interface ReadFileBlockingActions {

    static Buffer readFileBlocking(Path path) {
        return vertx
                .fileSystem()
                .readFileBlocking(path.toString());
    }

    static Buffer fileChannel(Path path) throws IOException {
        int size = (int) path.toFile().length();
        int bufferSize = 64 * 1024;

        byte[] result = new byte[size];
        try (FileChannel channel = FileChannel.open(path)) {
            ByteBuffer buffer = ByteBuffer.allocate(bufferSize);

            int position = 0;
            while (position < size) {
                channel.read(buffer, position);
                buffer.flip();
                System.arraycopy(buffer.array(), 0, result, position, min(bufferSize, size - position));
                position += bufferSize;
            }
        }
        return Buffer.buffer(result);
    }

}