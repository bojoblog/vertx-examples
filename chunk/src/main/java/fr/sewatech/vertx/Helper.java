package fr.sewatech.vertx;

import java.lang.management.ManagementFactory;

public interface Helper {
    static String message(Object content) {
        long uptime = ManagementFactory.getRuntimeMXBean().getUptime();
        String threadName = Thread.currentThread().getName();
        long usedMemory = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() / 1024 / 1024;

        return String.format("%s - [%s] %s (%s MB)", uptime, threadName, content, usedMemory);
    }

    static void println(Object content) {
        System.out.println(message(content));
    }

}
