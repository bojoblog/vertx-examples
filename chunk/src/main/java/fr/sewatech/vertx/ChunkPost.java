package fr.sewatech.vertx;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import static fr.sewatech.vertx.ChunkApplication.DELAY;
import static fr.sewatech.vertx.ChunkApplication.MAX_CHUNKS;
import static fr.sewatech.vertx.Helper.println;

class ChunkPost {
    private final Vertx vertx;

    ChunkPost(Vertx vertx) {
        this.vertx = vertx;
    }

    void mainPost() {
        startServer(ar -> startClient(ar.result().actualPort()));
    }

    private void startServer(Handler<AsyncResult<HttpServer>> listenHandler) {
        Handler<HttpServerRequest> requestHandler =
                request -> request
                        .handler(buffer -> println("Server receive: " + buffer))
                        .response()
                        .end("Request sent");

        vertx.createHttpServer()
                .requestHandler(requestHandler)
                .listen(ChunkApplication.PORT, listenHandler);
    }

    private void startClient(int port) {
        HttpClientRequest request = vertx.createHttpClient().post(port, "localhost", "/")
                .setChunked(true)
                .handler(response -> {
                    println("Client: reponse...");
                    response.handler(buffer -> println("Client receive: " + buffer));
                });

        AtomicInteger counter = new AtomicInteger(0);
        vertx.setPeriodic(DELAY, id -> {
            if (counter.get() >= MAX_CHUNKS) {
                request.end("end");
                vertx.cancelTimer(id);
                vertx.close();
            } else {
//                char[] fill = new char[255];
//                Arrays.fill(fill, '=');
//                String message = "chunk n°" + counter.incrementAndGet() + String.valueOf(fill);
                String message = "{\"bytesAsBase64\":\"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=\",\"value\":0}";
                println("Client send: " + message);
                request.write(message);
            }
        });
    }
}
