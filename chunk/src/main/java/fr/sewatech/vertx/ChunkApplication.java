package fr.sewatech.vertx;

import io.vertx.core.Vertx;

import java.util.stream.Stream;

public interface ChunkApplication {
    Vertx vertx = Vertx.vertx();

    int MAX_CHUNKS = 100;
    long DELAY = 10L;
    int PORT = 9001;

    static void main(String[] args) {
        vertx.exceptionHandler(throwable -> {
            throwable.printStackTrace();
            vertx.close();
        });

        RunMode.from(args).main.run();
    }

    enum RunMode {
        GET(new ChunkGet(vertx)::mainGet),
        POST(new ChunkPost(vertx)::mainPost);

        private final Runnable main;

        RunMode(Runnable main) {
            this.main = main;
        }

        static RunMode from(String[] args) {
            return Stream.of(args)
                    .findFirst()
                    .map(text -> text.replace('-', '_'))
                    .map(String::toUpperCase)
                    .map(RunMode::valueOf)
                    .orElse(GET);
        }
    }
}
