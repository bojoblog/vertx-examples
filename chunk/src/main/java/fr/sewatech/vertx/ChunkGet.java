package fr.sewatech.vertx;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;

import java.util.concurrent.atomic.AtomicInteger;

import static fr.sewatech.vertx.ChunkApplication.*;
import static fr.sewatech.vertx.Helper.println;

class ChunkGet {
    private final Vertx vertx;

    ChunkGet(Vertx vertx) {
        this.vertx = vertx;
    }

    void mainGet() {
        startGetServer(ar -> startGetClient(ar.result().actualPort()));
    }

    private void startGetServer(Handler<AsyncResult<HttpServer>> listenHandler) {
        Handler<HttpServerRequest> requestHandler = request -> {
            request.response().setChunked(true);
            AtomicInteger counter = new AtomicInteger(0);
            vertx.setPeriodic(DELAY, id -> {
                if (request.response().closed() || counter.get() >= MAX_CHUNKS) {
                    println("Server finished");
                    request.response().end("last chunk");
                    vertx.cancelTimer(id);
                } else {
                    String message = "chunk n°" + counter.incrementAndGet();
                    println("Server send: " + message);
                    request.response().write(message);
                }
            });
        };

        vertx.createHttpServer()
                .requestHandler(requestHandler)
                .listen(PORT, listenHandler);
    }

    private void startGetClient(int port) {
        vertx.createHttpClient()
                .get(port, "localhost", "/")
                .handler(response -> {
                    println("Client: reponse...");
                    response.bodyHandler(event -> {
                        println("Client end");
                        vertx.close();
                    });
                    response.handler(buffer -> println("Client receive: " + buffer));
                    // handler(...) pour chunked response, bodyHandler(...) pour regular response
                    // Respecter l'ordre
                })
                .end();
    }
}
