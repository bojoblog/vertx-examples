package fr.sewatech.vertx.files;

import io.vertx.core.Promise;

import java.io.IOException;

@FunctionalInterface
public interface IOFunction<T, R> {
   R apply(T t) throws IOException;

   default void applyWithPromise(T param, Promise<R> promise) {
      try {
         promise.complete(this.apply(param));
      } catch (Exception e) {
         promise.fail(e);
      }
   }

}