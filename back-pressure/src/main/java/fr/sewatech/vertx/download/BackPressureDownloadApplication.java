package fr.sewatech.vertx.download;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;

import java.nio.file.Path;
import java.util.Optional;
import java.util.function.BiConsumer;

import static fr.sewatech.vertx.common.Helper.message;
import static fr.sewatech.vertx.download.BackPressureDownloadActions.EXCEPTION_HANDLER;
import static fr.sewatech.vertx.download.BackPressureDownloadActions.notFound;
import static fr.sewatech.vertx.files.BigFiles.getPath;
import static fr.sewatech.vertx.files.BigFiles.initBigFiles;

public class BackPressureDownloadApplication {

    static final Vertx vertx = Vertx.vertx();
    private static final BackPressureDownloadApplication singleton = new BackPressureDownloadApplication();

    public static void main(String[] args) {
        singleton.go();
    }

    private void go() {
        initBigFiles();

        vertx.exceptionHandler(EXCEPTION_HANDLER);
        vertx.createHttpServer()
                .requestHandler(this::download)
                .exceptionHandler(EXCEPTION_HANDLER)
                .listen(8000,
                        ar -> {
                            if (ar.succeeded()) {
                                System.out.println("Server ready on port " + ar.result().actualPort());
                            } else {
                                System.err.println("Error on starting server " + ar.cause());
                            }
                        });
    }

    private void download(HttpServerRequest request) {
        Path path = getPath("-" + request.path().substring(1));
        DownloadMode mode = DownloadMode.of(request);
        System.out.println(message(String.format("Start: %s in mode %s", path.getFileName(), mode)));

        if (!vertx.fileSystem().existsBlocking(path.toString())) {
            notFound(request);
            return;
        }

        mode.download(path, request);
    }

    enum DownloadMode {
        READ(BackPressureDownloadActions::downloadRead),
        NO_BP((path, request) -> BackPressureDownloadActions.downloadChunked(path, request, false)),
        BP((path, request) -> BackPressureDownloadActions.downloadChunked(path, request, true)),
        PIPE(BackPressureDownloadActions::downloadPipe),
        SEND(BackPressureDownloadActions::downloadSendFile),
        DEFAULT((path, request) -> BackPressureDownloadActions.notFound(request));

        private final BiConsumer<Path, HttpServerRequest> function;

        DownloadMode(BiConsumer<Path, HttpServerRequest> function) {
            this.function = function;
        }

        static DownloadMode of(HttpServerRequest request) {
            String mode = request.getParam("mode");
            return Optional.of(mode)
                    .map(text -> text.replace('-', '_'))
                    .map(String::toUpperCase)
                    .map(DownloadMode::valueOf)
                    .orElse(DEFAULT);
        }

        public void download(Path path, HttpServerRequest request) {
            function.accept(path, request);
        }
    }

}
