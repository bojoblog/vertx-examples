package fr.sewatech.vertx.download;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;

import java.nio.file.Path;

import static fr.sewatech.vertx.common.Helper.message;
import static fr.sewatech.vertx.download.BackPressureDownloadApplication.vertx;

public interface BackPressureDownloadActions {

    Handler<Throwable> EXCEPTION_HANDLER = Throwable::printStackTrace;

    static void downloadRead(Path path, HttpServerRequest request) {
        HttpServerResponse response = request.response();
        vertx
                .fileSystem()
                .readFile(
                        path.toString(),
                        ar -> {
                            if (ar.succeeded()) {
                                response
                                        .exceptionHandler(EXCEPTION_HANDLER)
                                        .endHandler(nothing -> System.out.println(message("Done: " + path.getFileName())))
                                        .putHeader("Content-Disposition", "attachment; filename=" + path.getFileName())
                                        .end(ar.result());
                            } else {
                                System.err.println(message("Error: " + ar.cause().toString()));
                                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end();
                            }
                        });
    }

    static void downloadChunked(Path path, HttpServerRequest request, boolean backPressure) {
        vertx
                .fileSystem()
                .open(
                        path.toString(),
                        new OpenOptions().setRead(true),
                        ar -> {
                            if (ar.succeeded()) {
                                downloadAsyncFileChunked(ar.result(), request, backPressure);
                            } else {
                                System.err.println(message("Error: " + ar.cause().toString()));
                                request.response().setStatusCode(500).end(ar.cause().toString());
                            }
                        });
    }

    static void downloadAsyncFileChunked(AsyncFile file, HttpServerRequest request, boolean backPressure) {
        HttpServerResponse response = request.response()
                .exceptionHandler(EXCEPTION_HANDLER)
                .setStatusCode(200)
                .setChunked(true);
        file
                .handler(buffer -> {
                    response.write(buffer);
                    if (backPressure && response.writeQueueFull()) {
                        file.pause();
                        response.drainHandler(nothing -> file.resume());
                    }
                })
                .endHandler(nothing -> {
                    response.end();
                    System.out.println(message("Done: " + request.path()));
                })
                .exceptionHandler(EXCEPTION_HANDLER);
    }

    static void downloadPipe(Path path, HttpServerRequest request) {
        vertx
                .fileSystem()
                .open(
                        path.toString(),
                        new OpenOptions().setRead(true),
                        ar -> {
                            if (ar.succeeded()) {
                                HttpServerResponse response = request.response()
                                        .exceptionHandler(EXCEPTION_HANDLER)
                                        .setStatusCode(200)
                                        .setChunked(true);

                                ar.result().pipeTo(
                                        response,
                                        arv -> {
                                            if (arv.succeeded()) {
                                                System.out.println(message("Done: " + request.path()));
                                            } else {
                                                arv.cause().printStackTrace();
                                            }
                                        });
                            } else {
                                System.err.println(message("Error: " + ar.cause().toString()));
                                request.response().setStatusCode(500).end(ar.cause().toString());
                            }
                        });
    }

    static void downloadSendFile(Path path, HttpServerRequest request) {
        request.response()
                .exceptionHandler(EXCEPTION_HANDLER)
                .sendFile(
                        path.toString(),
                        ar -> {
                            if (ar.succeeded()) {
                                System.out.println(message("Done: " + request.path()));
                            } else {
                                EXCEPTION_HANDLER.handle(ar.cause());
                            }
                        }
                );
    }

    static void notFound(HttpServerRequest request) {
        System.out.println(HttpResponseStatus.NOT_FOUND);
        request.response().setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end();
    }
}
