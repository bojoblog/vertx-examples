package fr.sewatech.vertx.count;

import fr.sewatech.vertx.common.Data;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.TimeoutStream;
import io.vertx.core.Vertx;
import io.vertx.core.streams.WriteStream;

import java.util.LinkedList;
import java.util.Queue;

import static fr.sewatech.vertx.common.Helper.println;

public class PrintStream implements WriteStream<Data> {

    private final Vertx vertx;

    private final Queue<Data> queue = new LinkedList<>();
    private int capacity = Integer.MAX_VALUE;

    private final TimeoutStream periodicStream;
    private boolean finished = false;
    private Handler<Void> drainHandler;

    public PrintStream(Vertx vertx) {
        this.vertx = vertx;
        periodicStream = vertx.periodicStream(50L)
                .handler(event -> consume());
    }

    private void consume() {
        if (queue.isEmpty()) {
            if (finished) {
                vertx.close();
            }
        } else {
            if (queue.size() < capacity / 2 && !finished) {
                drain();
            }
            println(queue.poll());
        }
    }

    private void drain() {
        if (drainHandler != null) {
            drainHandler.handle(null);
        }
    }

    @Override
    public WriteStream<Data> exceptionHandler(Handler<Throwable> handler) {
        return this;
    }

    @Override
    public WriteStream<Data> write(Data data) {
        queue.add(data);
        return this;
    }

    @Override
    public WriteStream<Data> write(Data data, Handler<AsyncResult<Void>> handler) {
        queue.add(data);
        return this;
    }

    @Override
    public void end() {
        this.finished = true;
        println("Completed");
    }

    @Override
    public void end(Handler<AsyncResult<Void>> handler) {
        end();
    }

    @Override
    public WriteStream<Data> setWriteQueueMaxSize(int maxSize) {
        this.capacity = maxSize;
        return this;
    }

    @Override
    public boolean writeQueueFull() {
        return queue.size() >= capacity;
    }

    @Override
    public WriteStream<Data> drainHandler(Handler<Void> handler) {
        drainHandler = handler;
        return this;
    }
}
