package fr.sewatech.vertx.count;

import fr.sewatech.vertx.common.Data;
import io.vertx.core.Vertx;
import io.vertx.core.streams.ReadStream;
import io.vertx.core.streams.WriteStream;

import java.util.stream.Stream;

public class BackPressureCountApplication {
    private static final BackPressureCountApplication singleton = new BackPressureCountApplication();

    private static final Vertx vertx = Vertx.vertx();

    public static void main(String[] args) {
        vertx.exceptionHandler(throwable -> {
            throwable.printStackTrace();
            vertx.close();
        });
        RunMode.from(args).main.run();
    }

    private void mainBounded() {
        ReadStream<Data> readStream = new DataStream(vertx);
        WriteStream<Data> writeStream = new PrintStream(vertx).setWriteQueueMaxSize(100);

        readStream
                .pipe()
                .to(writeStream);
    }

    private void mainUnbounded() {
        ReadStream<Data> readStream = new DataStream(vertx);
        WriteStream<Data> writeStream = new PrintStream(vertx).setWriteQueueMaxSize(Integer.MAX_VALUE);

        readStream
                .pipe()
                .to(writeStream);
    }

    private enum RunMode {
        BOUNDED(BackPressureCountApplication.singleton::mainBounded),
        UNBOUNDED(BackPressureCountApplication.singleton::mainUnbounded);

        private final Runnable main;

        RunMode(Runnable main) {
            this.main = main;
        }

        static RunMode from(String[] args) {
            return Stream.of(args)
                    .findFirst()
                    .map(text -> text.replace('-', '_'))
                    .map(String::toUpperCase)
                    .map(RunMode::valueOf)
                    .orElse(BOUNDED);
        }
    }

}
