package fr.sewatech.vertx.count;

import fr.sewatech.vertx.common.Data;
import io.vertx.core.Handler;
import io.vertx.core.TimeoutStream;
import io.vertx.core.Vertx;
import io.vertx.core.streams.ReadStream;

import static fr.sewatech.vertx.common.Helper.println;

public class DataStream implements ReadStream<Data> {
    private static final int MAX_COUNT = 1_000;

    private final TimeoutStream periodicStream;
    private Handler<Void> endHandler;

    public DataStream(Vertx vertx) {
        periodicStream = vertx.periodicStream(10L);
    }

    @Override
    public synchronized DataStream fetch(long amount) {
        periodicStream.fetch(amount);
        return this;
    }

    @Override
    public DataStream exceptionHandler(Handler<Throwable> handler) {
        periodicStream.exceptionHandler(handler);
        return this;
    }

    @Override
    public synchronized DataStream handler(Handler<Data> handler) {
        periodicStream.handler(event -> produce(handler));
        return this;
    }

    private void produce(Handler<Data> handler) {
        Data data = new Data();
        if (data.index > MAX_COUNT) {
            periodicStream.cancel();
            endHandler.handle(null);
        } else {
            println("New data: " + data.index);
            handler.handle(data);
        }
    }

    @Override
    public synchronized DataStream pause() {
        periodicStream.pause();
        return this;
    }

    @Override
    public synchronized DataStream resume() {
        periodicStream.resume();
        return this;
    }

    @Override
    public synchronized DataStream endHandler(Handler<Void> endHandler) {
        this.endHandler = endHandler;
        return this;
    }
}
