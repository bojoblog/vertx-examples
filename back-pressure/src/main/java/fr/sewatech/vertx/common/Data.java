package fr.sewatech.vertx.common;

import java.util.Base64;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

public class Data {
    private static final AtomicLong counter = new AtomicLong(0);

    public final String content;
    public final Long index;

    public Data() {
        this(49_876);
    }

    public Data(int contentSize) {
        this.index = counter.getAndIncrement();

        byte[] bytes = new byte[contentSize];
        ThreadLocalRandom.current().nextBytes(bytes);
        content = Base64.getUrlEncoder().encodeToString(bytes).replace('\n', '_');
    }

    @Override
    public String toString() {
        return "data: " + index;
    }

    public String toJson() {
        return String.format("{ \"index\": %s, \"content\": \"%s\"}", index, content);
    }
}
