package fr.sewatech.vertx.common;

import java.lang.management.ManagementFactory;

public class Helper {
    public static void println(Object content) {
        System.out.println(message(content));
    }

    public static void sleep(long duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static String message(Object content) {
        long uptime = ManagementFactory.getRuntimeMXBean().getUptime();
        String threadName = Thread.currentThread().getName();
        long usedMemory = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() / 1024 / 1024;

        return String.format("%s - [%s] %s (%s MB)", uptime, threadName, content, usedMemory);
    }

}
